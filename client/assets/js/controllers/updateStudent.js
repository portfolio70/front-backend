/*
	Goals
	1. Display specific task that we have
		window.location.search returns query part of address
	2. Update specific record
*/

console.log(window.parent.location);
// will return the taskList._id part of the URL; result is file:///C:/Users/Ycoy%20Bontia/Documents/pacita-bontia/s14/client/pages/update-tasks.html?taskId=5f6c3e445ef4985e1cc9d143
//console.log(window.location.search);

// to get only the taskList_id, use URLSearchParams() -> object to help execute methods to access specific parts of the result ?taskid=5f6c3e445ef4985e1cc9d143 
/*	steps */
// 1. instantiate object URLSearchParams with window.location.search as param
let params = new URLSearchParams(window.location.search)
// check if worked
console.log(params);

// has() method checks if params key exists in the URL query string; true means key exists
// taskId exists in url because it has taskId value in the URL
console.log(params.has('studentID'))

// get() method returns the value of the key passed in an argument
console.log(params.get('studentID'))

// create var to assign value which you can use for findbyIdandUpdate
let studentID = params.get('studentID')

// declare vars that would target the elements that would contain the data you're going to get from the collection
let firstName = document.getElementById('firstName')
let lastname = document.getElementById('lastname')
let emailAddress = document.getElementById('emailAddress')
let mobileNumber = document.getElementById('mobileNumber')
let status = document.getElementById('status')
let courseId = document.getElementById('courseId')
let enrolmentDate = document.getElementById('enrolmentDate')

// declare vars in case user would like to update their details
let studentEmail = document.getElementById('emailVal')
let studentFirstName = document.getElementById('firstNameVal')
let studentLastName = document.getElementById('lastNameVal')
let studentMobileNumber = document.getElementById('mobileVal')

// fetch specific items 
fetch('https://stormy-falls-57077.herokuapp.com/api/students')
.then(response => response.json())
.then(data => {
	//console.log(data)
	let specificStudent = data.map(studentInfo => {
		if (studentInfo._id == studentID) {
			//console.log(studentInfo) //to check if correct output
			// following to fill out user details
			firstName.innerHTML = `First name: ${studentInfo.firstName}`
			lastname.innerHTML = `Last name: ${studentInfo.lastName}`
			emailAddress.innerHTML = `Email Address: ${studentInfo.emailAddress}`
			mobileNumber.innerHTML = `Mobile number: ${studentInfo.mobileNumber}`
			status.innerHTML = `Status: ${studentInfo.status}`
			courseId.innerHTML = `Current course: ${studentInfo.courseID}`
			enrolmentDate.innerHTML = `Next course (dd/mm/yyyy): ${studentInfo.enrolmentDate[0].enrolmentDay}/${studentInfo.enrolmentDate[0].enrolmentMonth}/${studentInfo.enrolmentDate[0].enrolmentYear}`

			//following to fill out fields in case user would like to update
			studentFirstName.defaultValue = `${studentInfo.firstName}`
			studentLastName.defaultValue = `${studentInfo.lastName}`
			studentEmail.defaultValue = `${studentInfo.emailAddress}`
			studentMobileNumber.defaultValue = `${studentInfo.mobileNumber}`
	
			return (studentInfo.studentID !== undefined) ? firstName.innerHTML = studentInfo.studentID : 'No student ID selected'
		}
	})
})

// to update specific details for furparent
// get form button id and add eventlistener to it
document.getElementById('putUserDetails').addEventListener('click', (e) => {
	// prevent default behavior of button click, which is to reload the page
	e.preventDefault();
	// taskID is not recognized by fetch, but api will, so use string literals
	// fetch = get, but update is for put, so add second parameter that'll indicate you'll use a method
	let studentUpdateEmail = emailVal.value
	let studentUpdateFirstName = firstNameVal.value
	let studentUpdateLastName = lastNameVal.value
	let studentUpdateMobileNumber = mobileVal.value

	fetch(`https://stormy-falls-57077.herokuapp.com/api/students/${studentID}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			firstName: studentUpdateFirstName,
			lastName: studentUpdateLastName,
			emailAddress: studentUpdateEmail,
			mobileNumber: studentUpdateMobileNumber
		})
	})
	.then(response => response.json())
	.then(data => {
		//console.log(data)
		if (data.message == "Furbaby details updated.") {
			alert('Furbaby details successfully updated.')
			//redirect to task index page
			window.location.replace(`./user-profile.html?studentID=${studentID}`)
			// to redirect to previously opened URL
			//window.history back()
		} else {
			alert('Something went wrong. Please try again later')
		}
	})
})

// deactivate user
document.getElementById('deactivateUser').addEventListener('click', (e) => {
	e.preventDefault();
	alert("Are you sure you want to delete your furbaby's profile?")
	alert("We'll keep your account active for 90 days in case you change your mind.")

	fetch(`https://stormy-falls-57077.herokuapp.com/api/students/delete/${studentID}/`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then(response => response.json())
	.then((data) => {
		if (data.message == 'Furbaby account deactivated. :(') {
			window.location.replace("../../index.html")
		} else {
			alert('Something went wrong.')
		}
	})
})


