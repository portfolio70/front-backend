let loggedInCredentials = localStorage.getItem("id")
//localStorage is web storage, for temporarily storing data

if(!loggedInCredentials || loggedInCredentials == null){
	let loginForm = document.getElementById('submitEmail-btn');

	loginForm.addEventListener('click', (e) => {
		e.preventDefault()

		// assign the values to variables below
		let emailAddress = document.getElementById('StudentemailAddress').value;
		let courseID = document.getElementById('userPassword').value;

		if(emailAddress == "" || courseID == "") {
			alert("Please input your email address and/or password.")
		}
		else {
			fetch('https://stormy-falls-57077.herokuapp.com/api/students/check', { //change to postman url
				// method is POST since you need to pass the values to the db in order to check them
				// if GET, it'll be carried in the URL
				method: 'POST',
				headers: { 
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					emailAddress: emailAddress,
					courseID: courseID,
				})
			})
			.then(res => {
				return res.json()
			})
			.then(data => {
				if (data.userLogin) {
          	//set the global user state to have properties containing authenticated user's ID and role
          	console.log(data.userLogin._id)

          	localStorage.setItem("id", data.userLogin._id)
          	localStorage.setItem("emailAddress", data.userLogin.emailAddress)
          	window.location.replace(`./client/pages/user-profile.html?studentID=${data.userLogin._id}`)
          } else {
          	alert("Oops! Something went wrong!")
          }

      })

		}
	})
} else {

}

/*
============== NOTES ===============
localStorage works only on client, not on api, so only available on front end
localStorage.setItem()
localStorage.getItem()
localStorage.removeItem() - remove 1 property inside local storage
localStorage.clear() - remove all properties inside localStorage

localStorage stores objects
------------------------------
Login page goals
1. get all the input fields needed for the login functionality
	- usually email and password\
2. using fetch(), collect the data from the api and use it on the localStorage
3. on your api, find the specific record that matches the input fields from the client
	- if there is a match, return the record as a response to the client
	- else return false 
4. Use the stored data as a reference on the loggedInCredential on all web pages 
*/