// let loggedInCredentials = localStorage.getItem("id")


// if (!loggedInCredentials || loggedInCredentials == null){
// 	alert("Login first!")
// } else {
// 	let firstNamelocal = localStorage.getItem("userEmail")
// }


	let userEmail = document.getElementById('userEmail')
	let userPwd = document.getElementById('userPwd')
	let firstName = document.getElementById('firstName')
	let lastName = document.getElementById('lastName')
	let mobileNumber = document.getElementById('mobileNumber')
	let courseID = document.getElementById('courseID')
	let status = document.getElementById('status')
	let enrolMonth = document.getElementById('enrolMonth')
	let enrolDay = document.getElementById('enrolDay')
	let enrolYear = document.getElementById('enrolYear')
	let studentRegForm = document.getElementById('createNewStudent')

// let object = {description:"Uwian na", teamID: "Team Uwian"}

// console.log(object);
// console.log(JSON.stringify(object))

studentRegForm.addEventListener('click', (e) => {
	e.preventDefault()
	// console.log(enrolMonth.value)
	// console.log(mobileNumber.value)

	let studentEmail = userEmail.value
	let studentPassword = userPwd.value
	let studentFirstName = firstName.value
	let studentLastName = lastName.value
	let studentMobileNumber = mobileNumber.value
	let studentCourse = courseID.value
	let studentStatus = status.value
	let studentEnrolMonth = enrolMonth.value
	let studentEnrolDay = enrolDay.value
	let studentEnrolYear = enrolYear.value

	fetch('https://stormy-falls-57077.herokuapp.com/api/students/register', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			firstName: studentFirstName,
			lastName: studentLastName,
			emailAddress: studentEmail,
			mobileNumber: studentMobileNumber,
			courseID: studentPassword, //will use this as pwd for the meantime
			status: studentStatus,
			enrolmentMonth: studentEnrolMonth,
			enrolmentDay: studentEnrolDay,
			enrolmentYear: studentEnrolYear
		})
	})
	.then(response => response.json())
	.then(data => {
		console.log(data)
		if (data.message == 'New furbaby info successfully added.') {
			alert(`New furbaby info successfully added! Welcome, ${studentFirstName}!`)
			window.location.replace(`./user-profile.html?studentID=${data._id}`)
		} else {
			alert('Server error! Please try again later')
		}
	})
})


// go to profile page
//logout button
let logoutBtn = document.getElementById('logoutBtn')
logoutBtn.addEventListener('click', (e)=> {
	localStorage.clear()
	window.location.replace('../index.html')
})