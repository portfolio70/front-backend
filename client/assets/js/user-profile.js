// event listeners for profile page

const updateBtn = document.getElementById('editUserDetails')
const btnContainer = document.getElementById('editDeacBtns')
const updatedForm = document.getElementById('updateStudentDetails')
const infoFromDb = document.getElementById('mainStudentInfo')
const cancelBtn = document.getElementById('cancelPut')

updateBtn.addEventListener('click', function(e){
	e.preventDefault()
	// for (var i = 0; i < updatedForm; i++) {
	// 	updatedForm.style.display = "flex"
	// }
	infoFromDb.style.display = "none"
	btnContainer.style.display = "none"
	updatedForm.style.display = "block"
})

let logoutBtn = document.getElementById('logoutBtn')
logoutBtn.addEventListener('click', (e)=> {
	localStorage.clear()
	window.location.replace('../index.html')
})

cancelBtn.addEventListener('click', function(e) {
	e.preventDefault()
	infoFromDb.style.display = "block"
	btnContainer.style.display = "block"
	updatedForm.style.display = "none"
})