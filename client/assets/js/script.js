// for eventlistener only
const loginFormDiv = document.getElementById('loginFormDiv')
const btns = document.getElementsByClassName('main-buttons')
const loginForm = document.getElementById('loginForm')
const cancelBtn = document.getElementById('cancel-btn')
const regBtns = document.getElementsByClassName('register-buttons')
const regForm = document.getElementById('regForm')
const loginBtn = document.getElementById('login-btn')

loginBtn.addEventListener('click', showLogin);

function showLogin() {
	for (var i = 0; i < btns.length; i++) {
		btns[i].style.display = "none"
		loginFormDiv.style.display = "block"
	}
}

cancelBtn.addEventListener("click", function(e){
	e.preventDefault()
	loginFormDiv.style = "none"
	for (var i = 0; i < btns.length; i++) {
		btns[i].style.display = "flex"
	}
})

