const loginForm = document.getElementById('loginForm')
const cancelBtn = document.getElementById('cancel-btn')
const regBtns = document.getElementsByClassName('register-buttons')
const regForm = document.getElementById('studentRegForm')


document.getElementById('register-go-btn').addEventListener('click', showRegForm);

function showRegForm() {
	for (var i = 0; i < regBtns.length; i++) {
		regBtns[i].style.display = "none"
		regForm.style.display = "block"
	}
}