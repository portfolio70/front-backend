// declare dependencies
// express
const express = require('express');
const app = express();
// mongooose & cors
const mongoose = require('mongoose');
const cors = require('cors');
const PORT = process.env.PORT || 5000;
const serverHost = process.env.HOST || '0.0.0.0';

app.use(cors());

// user collection schema
const User = require('./models/student');
// db connection
mongoose.connect('mongodb+srv://b65-csp2:csp2@zuitt-coding-bootcamp.r8hos.mongodb.net/b65?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
}); //b65-task-tracker

// error message if mongodb loses connection
mongoose.connection.on('error', () => {
	console.log('Connection error');
});
// if connection successful
mongoose.connection.once('open', () => {
	console.log('Now connected to cloud db server');
});

// handling json req, res from api
app.use(express.json());
// handling form data
app.use(express.urlencoded({extended:true}));

// route for user controller - ERROR FOR NOW
const userRoutes = require('./routes/student');
app.use('/api/students', userRoutes);
// users used to be students

// run server and initialize
app.listen(PORT, '0.0.0.0', () => {
	console.log(`Now listening for requests on port ${PORT}`);
});