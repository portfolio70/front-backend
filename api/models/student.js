// connect to mongoose
const mongoose = require('mongoose');

// structure for user-db
// firstName, lastName, email, password, isAdmin, mobileNo, enrollments{courseId, enrolmentDate, status} = all required
// timestamps
const userDbSchema = new mongoose.Schema ({
	firstName: { type: String, required: function () { return this.isActive } },
	lastName: { type: String, required: function () { return this.isActive } },
	emailAddress: { type: String, required: function () { return this.isActive } },
	isAdmin: { type: Boolean, default: false, required: [true, 'Please confirm if admin user.'] },
	mobileNumber: { 
		type: String,
		validate: {
			validator: function(v) {
				return /\d{12}/.test(v);
			},
			message: props => `${props.value} should be in 63xxxxxxxxxx format`
		},
		required: function () { return this.isActive }
		},
	courseID: { type: String },
	enrolmentDate: [
		{
		enrolmentMonth: { type: Number },
		enrolmentDay: { type: Number },
		enrolmentYear: {type: Number }
		}
		],
	status: { type: String },
	isActive: { type: Boolean, default: true }
	},{
		timestamps: true
	}
);

// export model
module.exports = mongoose.model('Student', userDbSchema);