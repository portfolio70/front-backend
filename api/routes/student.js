// dependencies
const express = require('express');
const router = express.Router();

// controller file for user
const UserController = require('../controllers/student')


// ====== CRUD FUNCTIONALITIES =================
// Profile page - get all student details
router.get('/', (req, res) => {
	UserController.getAllUsers().then(allUsers => res.send(allUsers))
})

// Profile page - get specific user details
router.get('/:studentID', (req, res) => {
	const studentID = req.params.studentID
	UserController.getStudent(studentID).then(result => res.send(result))
})

// Login page - add new user
router.post('/register', (req, res) => {
	let userInfo = req.body
	UserController.registerUser(userInfo).then(result => 
	{
		if (result == true) {
			return res.status(201).json({
				message: `New furbaby info successfully added.`
			})
		} else {
			return res.status(403).json({
				message: `Unable to add furbaby.`
			})
		}
	})
})
// login
// router.post('/register', (req, res) => {
// 	UserController.register(req.body).then(result => {
// 		if (result) {
// 			res.send(result)
// 		} else {
// 			return res.status(500).json({
// 				message: 'server error'
// 			})
// 		}
// 	})
// })

// ===== from repo for login page ======
router.post('/check', (req, res) => {
	UserController.getUser(req.body).then(result => res.send(result))
})

// update user
router.put('/:studentID', (req, res) => {
	const studentID = req.params.studentID
	UserController.updateStudentInfo(req.body, studentID).then(result => {
		if (result) {
			return res.status(200).json({
				message: 'Furbaby details updated.'
			})
		} else {
			return res.status(403).json({
				message: 'Unable to update furbaby details.'
			})
		}
	})
})

// deactivate user
router.put('/delete/:studentID', (req, res) => {
	const studentID = req.params.studentID
	UserController.deactivateUser(req.body, studentID).then(result => {
		if (result) {
			return res.status(200).json({
				message: 'Furbaby account deactivated. :('
			})
		} else {
			return res.status(403).json({
				message: 'Failed to deactivate furbaby account.'
			})
		}
	})
})

module.exports = router