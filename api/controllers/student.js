const Student = require('../models/student');

// get all users
module.exports.getAllUsers = (req, res) => {
	return Student.find({}).then(students => students)
}

// create new student info //s/b in body - raw
module.exports.registerUser = (params) => {
	if(params.firstName !=="" && params.lastName !=="") {
		let newStudent = new Student ({
			firstName: params.firstName,
			lastName: params.lastName,
			emailAddress: params.emailAddress,
			isAdmin: params.isAdmin,
			mobileNumber: params.mobileNumber,
			courseID: params.courseID,
			status: params.status,
			enrolmentDate: [{
				enrolmentMonth: params.enrolmentMonth,
				enrolmentDay: params.enrolmentDay,
				enrolmentYear: params.enrolmentYear
			}],
		})
		return newStudent.save().then((student, err) => {
			return (err) ? false : true
		})
	}
}

// show and prompt for update student details
module.exports.getStudent = (studentID) => {
	return Student.findById(studentID).then((specificStudent => specificStudent))
}

// for login page - from repo
module.exports.getUser = (userInfo) => {
		// req.params, preserved values from the router
	// res.send(req.params.userID)
	// return User.findById(userID).then((user, err)=> {
	// 	return (err)? true : false
	// })
	// User.findById(req.params.userID).then((user) => {
	// 	res.send(user)
	// })
	// findOne because not sure if user is registered
	return Student.findOne({emailAddress: userInfo.emailAddress}).then(user => {
		if (user === null) { //firstName here is like emailAddress
			return false
		} 
		//if data in 'user' (response of user.findone above) is null, then return 
		if (user.courseID == userInfo.courseID) { //lastName here is like password
			return {userLogin: user} //userLogin here is pikachu; this will return object that contains the user information, aka profile page
		} else {
			return false
		}
	})
}

// update studentDetails
module.exports.updateStudentInfo = (studentUpdateInfo, studentID) => {

		let studentUpdate = {
			$set: {
			firstName: studentUpdateInfo.firstName,
			lastName: studentUpdateInfo.lastName,
			emailAddress: studentUpdateInfo.emailAddress,
			mobileNumber: studentUpdateInfo.mobileNumber
		}
	}
		
		const options = {
			new: true
		}

		return Student.findByIdAndUpdate(studentID, studentUpdate, options).then((specificStudent, err) => {
				return (err) ? false : true
			})
}

// deactivate user
module.exports.deactivateUser = (studentArchive, studentID) => {

	let studentarchive = {
		$set: {
			isActive: false 
		}
	}
	const options = {
		new: true
	}

	return Student.findByIdAndUpdate(studentID, studentarchive, options).then((specificUser, err) => {
			return (err) ? 'Account has been deactivated' : 'Something went wrong.'
		})
	}
